<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>

<?php // $view->dump($view->getFlash()) ?>
<?php foreach ($view->getFlash() as $flash) {
echo '<p class="'.$flash['type'].'">'.$flash['message'].'</p>';
} ?>
<body>
<aside id="masthead">

    <nav>
        <ul>
            <li><a href="<?= $view->path(''); ?>">Home</a></li>
            <li><a href="<?= $view->path('listing'); ?>">Abonnés</a></li>
            <li><a href="<?= $view->path('products'); ?>">Products</a></li>
        </ul>
    </nav>
</aside>
<div class="container">
    <?= $content; ?>
</div>

<?php echo $view->add_webpack_script('admin'); ?>
<footer id="colophon">
    <div class="wrap">
        <p>MVC 6 - Framework Pédagogique.</p>
    </div>
</footer>
</body>



</html>
