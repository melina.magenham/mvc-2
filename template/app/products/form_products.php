<form method="post" action="" class="wrapform" novalidate>
    <?php echo $form->label('titre'); ?>
    <?php echo $form->input('titre'); ?>
    <?php echo $form->error('titre'); ?>

    <?php echo $form->label('ref'); ?>
    <?php echo $form->input('ref'); ?>
    <?php echo $form->error('ref'); ?>

    <?php echo $form->label('description'); ?>
    <?php echo $form->input('description'); ?>
    <?php echo $form->error('description'); ?>

    <?php echo $form->submit('submitted'); ?>
</form>
