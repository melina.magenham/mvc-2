<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProductsModel extends AbstractModel {
    protected static $table = 'products';

    protected $id;
    protected $titre;
    protected $ref;
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    public static function update($id,$post)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET titre = ?, reference = ?, description = ?, WHERE id = ?",
            array($post['titre'], $post['ref'], $post['description'],$id)
        );
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (titre, reference, description) VALUES (?,?,?)",
            array($post['titre'], $post['ref'],$post['description'])
        );
    }
}