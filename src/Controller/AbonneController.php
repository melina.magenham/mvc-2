<?php
namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\BaseModel;
use App\Model\ProductsModel;
use App\Service\Form;
use App\Service\Validation;

class AbonneController extends BaseController
{


    public function listing()
    {

        $this->render('app.abonnes.listing', array(
            'abonnes' => AbonneModel::all(),
        ), 'admin');
    }

    public function edit($id)
    {
        $this->getCategoryByIdOr404($id);

        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $v = new Validation();
            $errors = $this->validateAbonne($v, $post);

            if ($v->isValid($errors)) {
                AbonneModel::update($id, $post);
                // Message flash
                $this->addFlash('success', 'Merci !');
                // redirection
                $this->redirect('listing');
            }
        }
        $form = new Form($errors);
        $this->render('app.abonnes.edit', array(
            'form' => $form,
        ), 'admin');
    }

    public function new()
    {
        $errors = [];
        if (!empty($_POST['submitted'])) {
            // Faille XSS.
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validateAbonne($v, $post);
            if ($v->isValid($errors)) {
                AbonneModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Merci!');
                // redirection
                $this->redirect('listing');
            }
        }
        $form = new Form($errors);
        $this->render('app.abonnes.new', array(
            'form' => $form,
            'abonnes' => AbonneModel::all(),
        ), 'admin');
    }

    public function single($id)
    {
        $abonnes = AbonneModel::findById($id);
        $this->render('app.abonnes.single', array(
            'abonnes' => $abonnes,
        ), 'admin');
    }

    public function delete($id) {
        $this->getCategoryByIdOr404($id);
        AbonneModel::delete($id);
        $this->addFlash('success', 'Effacer!');
        $this->redirect('listing');
    }

    private function getCategoryByIdOr404($id)
    {
        $ab = AbonneModel::findById($id);
        if(empty($ab)) {
            $this->Abort404();
        }
        return $ab;
    }

    private function validateAbonne($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',2, 100);
        $errors['prenom'] = $v->textValid($post['prenom'], 'prenom',5, 500);
        $errors['email'] = $v->emailValid($post['email'], 'email');
        $errors['age'] = $v->textValid($post['age'], 'age',1 , 3);
        return $errors;
    }
}