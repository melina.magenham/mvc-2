<?php
namespace App\Controller;


use App\Model\ProductsModel;
use App\Service\Form;
use App\Service\Validation;

class ProductsController extends BaseController{

    public function products(){
        $this->render('app.products.listing', array(
        'products' => ProductsModel::all(),
        ), 'admin');
    }

    public function edit($id){
        $this->getCategoryByIdOr404($id);

        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $v = new Validation();
            $errors = $this->validateProducts($v, $post);

            if ($v->isValid($errors)) {
                ProductsModel::update($id, $post);
                // Message flash
                $this->addFlash('success', 'Merci !');
                // redirection
                $this->redirect('products');
            }
        }
        $form = new Form($errors);
        $this->render('app.products.editing', array(
        'form' => $form,
        ), 'admin');
    }

    public function new(){
        $errors = [];
        if(!empty($_POST['submitted'])) {
        // Faille XSS.
        $post = $this->cleanXss($_POST);
        // Validation
        $v = new Validation();
        $errors = $this->validateProducts($v,$post);
        if($v->isValid($errors)) {
            ProductsModel::insert($post);
            // Message flash
            $this->addFlash('success', 'Merci!');
            // redirection
            $this->redirect('products');
        }
    }
        $form = new Form($errors);
        $this->render('app.products.new', array(
        'form' => $form,
        ), 'admin');
    }

    public function single($id){
        $products = ProductsModel::findById($id);
        $this->render('app.products.single', array(
        'products' => $products,
        ), 'admin');
    }

    public function delete($id) {
        $this->getCategoryByIdOr404($id);
        ProductsModel::delete($id);
        $this->addFlash('success', 'Effacer!');
        $this->redirect('products');
    }

    private function getCategoryByIdOr404($id)
    {
        $pro = ProductsModel::findById($id);
        if(empty($pro)) {
            $this->Abort404();
        }
        return $pro;
    }

    private function validateProducts($v,$post)
    {
        $errors = [];
        $errors['titre'] = $v->textValid($post['titre'], 'titre',2, 100);
        $errors['ref'] = $v->textValid($post['ref'], 'ref',5, 500);
        $errors['description'] = $v->textValid($post['description'], 'description', 5,500);
        return $errors;
    }
}